#include "EDVS128_2106.h"

//#define DEFAULT_BIAS_SET		0			// "BIAS_DEFAULT"
//#define DEFAULT_BIAS_SET		1			// "BIAS_BRAGFOST"
//#define DEFAULT_BIAS_SET		2			// "BIAS_FAST"
//#define DEFAULT_BIAS_SET		3			// "BIAS_STEREO_PAIR"
//#define DEFAULT_BIAS_SET		4			// "BIAS_MINI_DVS"
#define DEFAULT_BIAS_SET		5			// "BIAS_BRAGFOST - ON/OFF Balanced"

// *****************************************************************************
extern unsigned char dataForTransmission[16];

// *****************************************************************************
unsigned long biasMatrix[12];

// *****************************************************************************
unsigned long enableEventSending;
unsigned long newEvent;
unsigned long x, y, p;

unsigned short eventBufferA[DVS_EVENTBUFFER_SIZE];		  // for event addresses
unsigned long  eventBufferT[DVS_EVENTBUFFER_SIZE];		  // for event time stamps
unsigned long eventBufferWritePointer;
unsigned long eventBufferReadPointer;

unsigned volatile long eventCounterTotal, eventCounterOn, eventCounterOff;

#ifdef INCLUDE_PIXEL_CUTOUT_REGION
unsigned long pixelCutoutMinX, pixelCutoutMaxX, pixelCutoutMinY, pixelCutoutMaxY;
#endif

extern long ledState;	 			// 0:off, -1:on, -2:blinking, >0: timeOn

// *****************************************************************************
#pragma interrupt_handler DVS128ChipCaptureEventISR
void DVS128ChipCaptureEventISR(void) {
  unsigned long DVSEvent;
  unsigned long DVSEventTime;

  DVSEvent = (FGPIO_IOPIN & PIN_ALL_ADDR) >> 16;			// fetch event
  DVSEventTime = T1_CR0;

#ifdef INCLUDE_PIXEL_CUTOUT_REGION
  {
	unsigned long pX = ((DVSEvent>>8) & 0x7F);
	unsigned long pY = ((DVSEvent)    & 0x7F);

	if ((pixelCutoutMinX <= pX) && (pixelCutoutMaxX >= pX) &&
	    (pixelCutoutMinY <= pY) && (pixelCutoutMaxY >= pY)) {
#endif

														// increase write pointer
  eventBufferWritePointer = ((eventBufferWritePointer+1) & DVS_EVENTBUFFER_MASK);
  eventBufferA[eventBufferWritePointer] = DVSEvent;   	// store event
  eventBufferT[eventBufferWritePointer] = DVSEventTime;	// store event time

  if (eventBufferWritePointer == eventBufferReadPointer) {
    eventBufferReadPointer = ((eventBufferReadPointer+1) & DVS_EVENTBUFFER_MASK);
    ledState=1000; LED_ON(); 		// indicate buffer overflow by LED (will turn out after some 10th of seconds)
#ifdef INCLUDE_MARK_BUFFEROVERFLOW
    eventBufferA[eventBufferWritePointer] |= OVERFLOW_BIT; // high bit set denotes buffer overflow
#endif
  }

  eventCounterTotal++;
  if (DVSEvent & MEM_ADDR_P) {
    eventCounterOff++;
  } else {
    eventCounterOn++;
  }

#ifdef INCLUDE_PIXEL_CUTOUT_REGION
    }
  }
#endif

  // acknowledge interrupt
  T1_IR = BIT(4);					// clear interrupt flag
  VICVectAddr = 0;					// Acknowledge Interrupt	(not for FIQ)
}

// *****************************************************************************
void DVS128ChipInit(void) {
  FGPIO_IOSET  = PIN_RESET_DVS;					// DVS array reset to high
  FGPIO_IODIR |= PIN_RESET_DVS;					// DVS array reset pin to output

#ifdef ACTIVE_DVS_ACKNOWLEDGE
#error  -- ACTIVE_DVS_ACKNOWLEDGE not implemented --
  FGPIO_IOSET  = PIN_DVS_ACKN;					// ackn to high
  FGPIO_IODIR |= PIN_DVS_ACKN;					// ackn to output port
#endif		  	 							// otherwise let DVS handshake itself, only grab addresses from bus

  FGPIO_IOSET  = (PIN_BIAS_LATCH);				// set pins to bias setup as outputs
  FGPIO_IOCLR  = (PIN_BIAS_CLOCK | PIN_BIAS_DATA);
  FGPIO_IODIR |= (PIN_BIAS_LATCH | PIN_BIAS_DATA | PIN_BIAS_CLOCK);

  FGPIO_IOCLR  = PIN_RESET_DVS;					// DVS array reset to low
  delayMS(10); 	 								// 10ms delay
  FGPIO_IOSET  = PIN_RESET_DVS;					// DVS array reset to high
  delayMS(1); 	 								// 1ms delay

  DVS128BiasLoadDefaultSet(DEFAULT_BIAS_SET);	// load default bias settings
  DVS128BiasFlush();							// transfer bias settings to chip

  // *****************************************************************************
  eventBufferWritePointer=0;					// initialize eventBuffer
  eventBufferReadPointer=0;

  // *****************************************************************************
  enableEventSending=0;
  DVS128FetchEventsEnable(FALSE);

#ifdef INCLUDE_PIXEL_CUTOUT_REGION
  pixelCutoutMinX = 0;
  pixelCutoutMaxX = 127;
  pixelCutoutMinY = 0;
  pixelCutoutMaxY = 127;
#endif


  // *****************************************************************************
  // ** initialize timer 0 (1ms clock)
  // *****************************************************************************
  T0_PR = (1000*PLL_CLOCK)-1;	// prescaler: run at 1ms clock rate
  T0_CTCR = 0x00;				// increase time on every T-CLK

  T0_MCR  = 0x00;				// match register, no special action, simply count until 2^32-1 and restart
  T0_TC	  = 0;					// reset counter to zero
  T0_TCR  = 0x01;				// enable Timer/Counter 0


  // *****************************************************************************
  // ** initialize timer 1 (system main clock)
  // *****************************************************************************
  T1_PR = 0;					// prescaler: run at main clock speed!
  T1_CTCR = 0x00;				// increase time on every T-CLK

//  T1_MR0 = BIT(16);			// match register: count only up to 2^16 = 0..65535
//  T1_MR0 = 50000;				// match register: count only up 2us * 50.000 = 100.000us = 100ms
//  T1_MCR  = 0x02;				// match register, reset counter on match with T1_MR0

  T1_MCR  = 0x00;				// match register, no special action, simply count until 2^32-1 and restart
//  T1_CCR  = BIT(1);				// capture TC in CR0 on falling edge of CAP1.0 (PIN_DVS_REQUEST)
  T1_CCR  = BIT(2)|BIT(1);		// capture TC in CR0 on falling edge of CAP1.0 (PIN_DVS_REQUEST) and generate interrupt
  PCB_PINSEL0 |= BIT(21);		// set P0.10 to capture register CAP1.0

  __DISABLE_INTERRUPT();
//  VICIntSelect = BIT(5);		// assign TIMER1 to FIQ group (0x00000020)	   	// seems this doesn't work with ImageCraft ICC

  VICVectAddr0=(unsigned)DVS128ChipCaptureEventISR;
  VICVectCntl0 = 0x20 | 5;
  VICIntEnable = 0x00000020;
  __ENABLE_INTERRUPT();

  T1_TC	  = 0;					// reset counter to zero
  T1_TCR  = 0x01;				// enable Timer/Counter 1
}

// *****************************************************************************
void DVS128FetchEventsEnable(unsigned char flag) {
  if (flag) {
    LEDSetOff();
    enableEventSending = 1;
  } else {
    LEDSetBlinking();
    enableEventSending = 0;
  }
}

// *****************************************************************************
void DVS128BiasSet(unsigned long biasID, unsigned long biasValue) {
  if (biasID < 12) {
    biasMatrix[biasID] = biasValue;
  }
}
// *****************************************************************************
unsigned long DVS128BiasGet(unsigned long biasID) {
  if (biasID < 12) {
    return(biasMatrix[biasID]);
  }
  return(0);
}

// *****************************************************************************
void DVS128BiasLoadDefaultSet(unsigned long biasSetID) {

  switch (biasSetID) {

  case 0:  // 12 bias values of 24 bits each 								BIAS_DEFAULT
    biasMatrix[ 0]=	    1067; // 0x00042B,	  		// Tmpdiff128.IPot.cas
    biasMatrix[ 1]=	   12316; // 0x00301C,			// Tmpdiff128.IPot.injGnd
    biasMatrix[ 2]=	16777215; // 0xFFFFFF,			// Tmpdiff128.IPot.reqPd
    biasMatrix[ 3]=	 5579732; // 0x5523D4,			// Tmpdiff128.IPot.puX
    biasMatrix[ 4]=	     151; // 0x000097,			// Tmpdiff128.IPot.diffOff
    biasMatrix[ 5]=	  427594; // 0x06864A,			// Tmpdiff128.IPot.req
    biasMatrix[ 6]=	       0; // 0x000000,			// Tmpdiff128.IPot.refr
    biasMatrix[ 7]=	16777215; // 0xFFFFFF,			// Tmpdiff128.IPot.puY
    biasMatrix[ 8]=	  296253; // 0x04853D,			// Tmpdiff128.IPot.diffOn
    biasMatrix[ 9]=	    3624; // 0x000E28,			// Tmpdiff128.IPot.diff
    biasMatrix[10]=	      39; // 0x000027,			// Tmpdiff128.IPot.foll
    biasMatrix[11]=        4; // 0x000004			// Tmpdiff128.IPot.Pr
    break;

  case 1:  // 12 bias values of 24 bits each 								BIAS_BRAGFOST
    biasMatrix[ 0]=        1067;	  		// Tmpdiff128.IPot.cas
    biasMatrix[ 1]=       12316;			// Tmpdiff128.IPot.injGnd
    biasMatrix[ 2]=    16777215;			// Tmpdiff128.IPot.reqPd
    biasMatrix[ 3]=     5579731;			// Tmpdiff128.IPot.puX
    biasMatrix[ 4]=          60;			// Tmpdiff128.IPot.diffOff
    biasMatrix[ 5]=      427594;			// Tmpdiff128.IPot.req
    biasMatrix[ 6]=           0;			// Tmpdiff128.IPot.refr
    biasMatrix[ 7]=    16777215;			// Tmpdiff128.IPot.puY
    biasMatrix[ 8]=      567391;			// Tmpdiff128.IPot.diffOn
    biasMatrix[ 9]=        6831;			// Tmpdiff128.IPot.diff
    biasMatrix[10]=          39;			// Tmpdiff128.IPot.foll
    biasMatrix[11]=           4;			// Tmpdiff128.IPot.Pr
    break;

  case 2:  // 12 bias values of 24 bits each 								BIAS_FAST
    biasMatrix[ 0]=        1966;	  		// Tmpdiff128.IPot.cas
    biasMatrix[ 1]=     1137667;			// Tmpdiff128.IPot.injGnd
    biasMatrix[ 2]=    16777215;			// Tmpdiff128.IPot.reqPd
    biasMatrix[ 3]=     8053457;			// Tmpdiff128.IPot.puX
    biasMatrix[ 4]=         133;			// Tmpdiff128.IPot.diffOff
    biasMatrix[ 5]=      160712;			// Tmpdiff128.IPot.req
    biasMatrix[ 6]=         944;			// Tmpdiff128.IPot.refr
    biasMatrix[ 7]=    16777215;			// Tmpdiff128.IPot.puY
    biasMatrix[ 8]=      205255;			// Tmpdiff128.IPot.diffOn
    biasMatrix[ 9]=        3207;			// Tmpdiff128.IPot.diff
    biasMatrix[10]=         278;			// Tmpdiff128.IPot.foll
    biasMatrix[11]=         217;			// Tmpdiff128.IPot.Pr
    break;

  case 3:  // 12 bias values of 24 bits each 								BIAS_STEREO_PAIR
    biasMatrix[ 0]=        1966;
    biasMatrix[ 1]=     1135792;
    biasMatrix[ 2]=    16769632;
    biasMatrix[ 3]=     8061894;
    biasMatrix[ 4]=         133;
    biasMatrix[ 5]=      160703;
    biasMatrix[ 6]=         935;
    biasMatrix[ 7]=    16769632;
    biasMatrix[ 8]=      205244;
    biasMatrix[ 9]=        3207;
    biasMatrix[10]=         267;
    biasMatrix[11]=         217;
    break;

  case 4:  // 12 bias values of 24 bits each 								BIAS_MINI_DVS
    biasMatrix[ 0]=        1966;
    biasMatrix[ 1]=     1137667;
    biasMatrix[ 2]=    16777215;
    biasMatrix[ 3]=     8053458;
    biasMatrix[ 4]=          62;
    biasMatrix[ 5]=      160712;
    biasMatrix[ 6]=         944;
    biasMatrix[ 7]=    16777215;
    biasMatrix[ 8]=      480988;
    biasMatrix[ 9]=        3207;
    biasMatrix[10]=         278;
    biasMatrix[11]=         217;
    break;

  case 5:  // 12 bias values of 24 bits each 								BIAS_BRAGFOST - on/off balanced
    biasMatrix[ 0]=        1067;	  		// Tmpdiff128.IPot.cas
    biasMatrix[ 1]=       12316;			// Tmpdiff128.IPot.injGnd
    biasMatrix[ 2]=    16777215;			// Tmpdiff128.IPot.reqPd
    biasMatrix[ 3]=     5579731;			// Tmpdiff128.IPot.puX
    biasMatrix[ 4]=          60;			// Tmpdiff128.IPot.diffOff
    biasMatrix[ 5]=      427594;			// Tmpdiff128.IPot.req
    biasMatrix[ 6]=           0;			// Tmpdiff128.IPot.refr
    biasMatrix[ 7]=    16777215;			// Tmpdiff128.IPot.puY
    biasMatrix[ 8]=      567391;			// Tmpdiff128.IPot.diffOn
    biasMatrix[ 9]=       19187;			// Tmpdiff128.IPot.diff
    biasMatrix[10]=          39;			// Tmpdiff128.IPot.foll
    biasMatrix[11]=           4;			// Tmpdiff128.IPot.Pr
    break;

  }
}

// *****************************************************************************
#pragma ramfunc DVS128BiasFlush
#define BOUT(x)  {if (x) FGPIO_IOSET = biasPIN; else FGPIO_IOCLR = biasPIN; FGPIO_IOSET = clockPIN; FGPIO_IOCLR = clockPIN; }

void DVS128BiasFlush(void) {
  unsigned long biasIndex, currentBias;
  unsigned long biasPIN, clockPIN;	   		// use local references to pins to save time
  		   				 					// the c compiler assigns up to four local registers (R4-R7),
											// so use them for four local variables
  biasPIN = PIN_BIAS_DATA;
  clockPIN = PIN_BIAS_CLOCK;

  for (biasIndex=0; biasIndex<12; biasIndex++) {
    currentBias = biasMatrix[biasIndex];

	BOUT(currentBias & 0x800000);
	BOUT(currentBias & 0x400000);
	BOUT(currentBias & 0x200000);
	BOUT(currentBias & 0x100000);
	
	BOUT(currentBias & 0x80000);
	BOUT(currentBias & 0x40000);
	BOUT(currentBias & 0x20000);
	BOUT(currentBias & 0x10000);
	
	BOUT(currentBias & 0x8000);
	BOUT(currentBias & 0x4000);
	BOUT(currentBias & 0x2000);
	BOUT(currentBias & 0x1000);
	
	BOUT(currentBias & 0x800);
	BOUT(currentBias & 0x400);
	BOUT(currentBias & 0x200);
	BOUT(currentBias & 0x100);

	BOUT(currentBias & 0x80);
	BOUT(currentBias & 0x40);
	BOUT(currentBias & 0x20);
	BOUT(currentBias & 0x10);

	BOUT(currentBias & 0x8);
	BOUT(currentBias & 0x4);
	BOUT(currentBias & 0x2);
	BOUT(currentBias & 0x1);

#ifdef NONO
	bitIndex = BIT(23);
	do {
	  if (currentBias & bitIndex) {
	    FGPIO_IOSET = PIN_BIAS_DATA;
	  } else {
	    FGPIO_IOCLR = PIN_BIAS_DATA;
	  }
	  FGPIO_IOSET = PIN_BIAS_CLOCK;

	  FGPIO_IOCLR = PIN_BIAS_CLOCK;

	  bitIndex >>= 1;
	} while (bitIndex);
#endif

  }  // end of biasIndexclocking

//  FGPIO_IOCLR = PIN_BIAS_DATA;	   // set data pin to low just to have the same output all the time

  // trigger latch to push bias data to bias generators
  FGPIO_IOCLR = PIN_BIAS_LATCH | PIN_BIAS_DATA;
  FGPIO_IOSET = PIN_BIAS_LATCH;
}


// *****************************************************************************
#pragma ramfunc DVS128BiasTransmitBiasValue
void DVS128BiasTransmitBiasValue(unsigned long biasID) {
  unsigned long biasValue;
  biasValue = biasMatrix[biasID];

  dataForTransmission[0] = (((biasValue)    ) & 0x3F) + 32;
  dataForTransmission[1] = (((biasValue)>> 6) & 0x3F) + 32;
  dataForTransmission[2] = (((biasValue)>>12) & 0x3F) + 32;
  dataForTransmission[3] = (((biasValue)>>18) & 0x3F) + 32;
  dataForTransmission[4] = biasID + 32;

  transmitSpecialData(5);
}
