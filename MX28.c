#include "EDVS128_2106.h"

/*

Dynamixel MX-28 servo driver

This code handles communication with a Dynamixel MX-28 servo.
Various data (e.g. goal/present position) can be send to or read from the
motor by sending packages.

Writing: A write request package is sent and the motor replies
		 with an empty package.
Reading: A read request package is sent and the motor replies
		 with a package containing the requested data.

Package structure:
		0xFF 0xFF ID LEN MODE ADDRESS DATA... CHECKSUM
	ID is 1 by default
	LEN is the number of data bytes + 2
	MODE is either 0: reply, 2: read request, or 3: write request
	ADDRESS is the motor register address (i.e. 30 for goal position)
	DATA data bytes
	CHECKSUM is the a checksum byte: ~(sum of all bytes ID to DATA)

The code is layout such that each request to read from or write to
the motor is formulated as a "request".
Requests are stored in a FIFO queue and executed one after another.
After sending a request the program awaits the proper answer package
and accordingly updates the requested value.

*/


// if the interface is active
long MX28_IsActive;

// default motor id to use (assuming only one motor)
#define MX28_DEFAULT_ID			(1)

// package modes
#define INST_READ				(2)
#define INST_WRITE				(3)

// buffer to cache bytes read from rx
#define MX28_RX_LEN 32
unsigned char MX28_rx_data[MX28_RX_LEN];
long MX28_rx_idx = 0;

// fifo queue for storing read/write requests
// items have the format:
//	 0..15: DATA
//	16..23: ADDRESS
//	24..27: MODE
//	28..31: (unused)
#define MX28_QUEUE_LEN 8
unsigned long MX28_queue[MX28_QUEUE_LEN];
long MX28_queue_idx;

// 0/1 if there is a current pending request
long MX28_request;
// current pending request
unsigned long MX28_request_item;

// some limits
#define MX28_POSITION_MIN		(0)
#define MX28_POSITION_MAX		(4095)
#define MX28_POSITION_CENTER	(2047)

// cached motor values
unsigned char MX28_var_value[73];

// 0/1 for auto movement (moving back and forth)
long MX28_auto;


void MX28_var_cache_set_byte(long address, unsigned char val)
{
	MX28_var_value[address] = val;
}

unsigned char MX28_var_cache_get_byte(long address)
{
	return MX28_var_value[address];
}

void MX28_var_cache_set_word(long address, unsigned char valL, unsigned char valH)
{
	MX28_var_value[address] = valL;
	MX28_var_value[address+1] = valH;
}

long MX28_var_cache_get_word(long address)
{
	long valL = MX28_var_value[address];
	long valH = MX28_var_value[address+1];
	return valL + (valH << 8);
}

void MX28_Init(void)
{
	// Enable the divisor, set 8N1 (1 0 00 0 0 11)
	UART1_LCR = 0x83;
	// set baudrate 57600
	UART1_DLM = 0x00; UART1_DLL = 0x36; UART1_FDR = (((0x07)<<4) | 0x02);
	// UART1_DLM = 0x00; UART1_DLL = 0x04; UART1_FDR = 0x00; // 1000000 baud
	// Close divisor, set 8N1 (0 0 00 0 0 11)
	UART1_LCR = 0x03;

	// disable UART1 interrupts
	UART1_IER = 0x00;
	// enable the fifos
	UART1_FCR = 0x01;
	// Reset FIFOs
	UART1_FCR = 0x01 | 0x06;
	// Enable Transmitter (default)
	UART1_TER = 0x80;

	// enable TxD1, RxD1 output pins
	PCB_PINSEL0 |= BIT(16) | BIT(18);

	// init internal parameters
	MX28_IsActive = FALSE;
	MX28_rx_idx = 0;
	MX28_queue_idx = 0;
	MX28_request = 0;
	MX28_auto = 0;

	// FIXME initialize motor variable cache with correct values?

	// drive motor to 0 degree
	MX28_Set(MX28_GOAL_POSITION, MX28_POSITION_CENTER);
}

void MX28_write_word_request(unsigned char address, unsigned long word)
{
	if(MX28_queue_idx < MX28_QUEUE_LEN) {
		MX28_queue[MX28_queue_idx] =
			((INST_WRITE & 0xF) << 24) |
			((address & 0xFF) << 16) |
			(word & 0xFFFF);
		MX28_queue_idx++;
	}
	else {
		// ERROR queue full!
		printf("MX28 ERR queue full\n");
	}
}

void MX28_read_word_request(unsigned char address)
{
	if(MX28_queue_idx < MX28_QUEUE_LEN) {
		MX28_queue[MX28_queue_idx] =
			((INST_READ & 0xF) << 24) |
			(address & 0xFF) << 16;
		MX28_queue_idx++;
	}
	else {
		// ERROR queue full!
		printf("MX28 ERR queue full\n");
	}
}

void MX28_send_byte(unsigned char mode, unsigned char address, unsigned char value)
{
	unsigned char cs;
	// assemble package
	UART1_THR = 0xff;		// header
	UART1_THR = 0xff;		// header
	UART1_THR = MX28_DEFAULT_ID;	// default id
	UART1_THR = 4;			// length
	UART1_THR = mode;		// mode
	UART1_THR = address;	// address
	UART1_THR = value;		// value
	cs = MX28_DEFAULT_ID+4+mode+address+value;
	UART1_THR = ~cs;		// checksum
	UART1_THR = 0x00;		// used for polling reply // FIXME ???
	UART1_THR = 0x00;		// used for polling reply
}

void MX28_send_word(unsigned char mode, unsigned char address, unsigned long value)
{
	long valL, valH;
	unsigned char cs;
	// assemble package
	valL = value & 0xFF;
	valH = (value >> 8) & 0xFF;
	UART1_THR = 0xff;		// header
	UART1_THR = 0xff;		// header
	UART1_THR = MX28_DEFAULT_ID;	// default id
	UART1_THR = 5;			// length
	UART1_THR = mode;		// mode
	UART1_THR = address;	// address
	UART1_THR = valL;		// value
	UART1_THR = valH;		// value
	cs = MX28_DEFAULT_ID+5+mode+address+valL+valH;
	UART1_THR = ~cs;		// checksum
	UART1_THR = 0x00;		// used for polling reply // FIXME ???
	UART1_THR = 0x00;		// used for polling reply
}

void MX28_send_request(unsigned long item)
{
	unsigned char mode;
	unsigned char address;
	// save for answer
	MX28_request_item = item;
	MX28_request = 1;
	// get data
	mode = (item >> 24) & 0xF;
	address = (item >> 16) & 0xFF;
	if(mode == INST_WRITE) {
		MX28_send_word(mode, address, item & 0xFFFF);
	}
	else if(mode == INST_READ) {
		// request two bytes
		MX28_send_byte(mode, address, 2);
	}
	else {
		// ERROR unknown
	}
}

extern long enableEventSending;
extern long enableUARTecho;

#define MX28_ADDRESS_NAME_HELPER(X) case X: return #X;

unsigned char* MX28_address_name(long address)
{
	switch(address) {
		MX28_ADDRESS_NAME_HELPER(MX28_GOAL_POSITION)
		MX28_ADDRESS_NAME_HELPER(MX28_MOVING_SPEED)
		MX28_ADDRESS_NAME_HELPER(MX28_TORQUE_LIMIT)
		MX28_ADDRESS_NAME_HELPER(MX28_PRESENT_POSITION)
		default: return "(unknown)";
	}
}

void MX28_handle_package(unsigned char* data, long len)
{
	unsigned char mode;
	unsigned char address;

	// long i;
	// printf("\nX (%d):", len);
	// for(i=0; i<len; i++) {
	// 	printf(" %u", (unsigned)(data[i]));
	// }
	// printf("\n");

	if(MX28_request == 0 || len == 0 || data[0] != 0) {
		// readback TX to RX or wrong package
		return;
	}
	// what was requested
	mode = (MX28_request_item >> 24) & 0xF;
	address = (MX28_request_item >> 16) & 0xFF;
	// check if we got the answer
	if(mode == INST_READ) {
		// FIXME assuming a word was requested
		if(len == 3 && data[0] == 0) {
			// cache variable
			MX28_var_cache_set_word(address, data[1], data[2]);
			if(MX28_auto == 0 && enableEventSending==0 && enableUARTecho>1) {
				printf("MX28 %d(%s)=%d\n", address, MX28_address_name(address), MX28_var_cache_get_word(address));
			}
			MX28_request = 0;
		}
	}
	else if(mode == INST_WRITE) {
		// expect package with length=1 and data=0
		if(len == 1 && data[0] == 0) {
			MX28_request = 0;
		}
	}
	else {
		// ERROR
	}
}

void MX28_Set(long address, long val)
{
	MX28_write_word_request(address, val);
	MX28_read_word_request(address);
}

void MX28_Request(long address)
{
	MX28_read_word_request(address);
}

long MX28_Get(long address)
{
	return MX28_var_cache_get_word(address);
}

void MX28_Add_RBR_Char(unsigned char c)
{
	// printf("(%d)", c);
	// check if cache is big enough
	if(MX28_rx_idx == MX28_RX_LEN) {
		// ERROR cache is full!
		printf("MX28 ERR RBR full\n");
	}
	else {
		// cache data
		MX28_rx_data[MX28_rx_idx] = c;
		// increment pointer
		MX28_rx_idx++;
	}
}

void MX28_Update_1(void)
{
	// parse RX
	if(4 <= MX28_rx_idx) {
		// packet structure:
		// 0: 0xFF
		// 1: 0xFF
		// 2: ID (1 by default)
		// 3: LEN (number of data bytes including byte 3)
		// 4..4+LEN-2: DATA
		// 4+LEN-1: CS packet checksum

		long parse_idx;
		long packet_idx;
		long packet_id;
		long packet_len;
		unsigned char cs;
		long i;

		parse_idx = 0;
		// parse packets
		while(parse_idx+4 <= MX28_rx_idx) { // minimal packet length is 4
			if(MX28_rx_data[parse_idx] == 0xFF && MX28_rx_data[parse_idx+1] == 0xFF) {
				packet_id = MX28_rx_data[parse_idx+2];
				packet_len = MX28_rx_data[parse_idx+3];
				// check for enough bytes
				if(parse_idx+4+packet_len <= MX28_rx_idx) {
					// check for header
					if(
						packet_id == MX28_DEFAULT_ID &&
						packet_len > 0 // at least 1 byte for checksum
					) {
//						printf("\nid=%d, len=%d\n", packet_id, packet_len);
						packet_idx = parse_idx+4;
						// compute checksum
						cs = packet_id + packet_len;
						for(i=0; i<packet_len-1; i++) { // data
							cs += MX28_rx_data[packet_idx+i];
						}
						cs = 255 - cs;
//						printf("\ncs: %u vs %d\n", cs, MX28_rx_data[packet_idx + packet_len-1]);
						// check checksum
						if(cs == MX28_rx_data[packet_idx + packet_len-1]) {
							// got a package!
							MX28_handle_package(MX28_rx_data + packet_idx, packet_len-1);
						}
						else {
							// wrong checksum -> ignore package
						}
						parse_idx = packet_idx + packet_len;
					}
				}
				else {
					// wait for more bytes
					break;
				}
			}
			else {
				parse_idx++;
			}
		}
		// remove data from cache
		if(parse_idx > 0) {
			for(i=parse_idx; i<MX28_rx_idx; i++) {
				MX28_rx_data[i-parse_idx] = MX28_rx_data[i];
			}
			MX28_rx_idx -= parse_idx;
		}
	}

	// send TX
	if(MX28_request == 0 && MX28_queue_idx > 0) {
		long i;
		MX28_send_request(MX28_queue[0]);
		for(i=1; i<MX28_queue_idx; i++) {
			MX28_queue[i-1] = MX28_queue[i];
		}
		MX28_queue_idx --;
	}

}

void MX28_Update_5(void)
{
	if(MX28_auto) {
		MX28_Request(MX28_PRESENT_POSITION);
	}
}

void MX28_Update_20(void)
{
	long pos_goal;
	long pos_present;
	long delta;

	if(MX28_auto) {
		// move back and forth from min to max

		pos_goal = MX28_Get(MX28_GOAL_POSITION);
		pos_present = MX28_Get(MX28_PRESENT_POSITION);
		delta = pos_goal - pos_present;
		if(delta < 0) delta = -delta;

		if(delta < 10) {
			// goal reached
			if(pos_goal == MX28_POSITION_MIN) {
				MX28_Set(MX28_GOAL_POSITION, MX28_POSITION_MAX);
			}
			else {
				MX28_Set(MX28_GOAL_POSITION, MX28_POSITION_MIN);
			}
		}
	}
}

extern long parseLong(char **c);

unsigned char toLower(unsigned char c) {
	if('A' <= c && c <= 'X') {
		return (c - 'A') + 'a';
	}
	else {
		return c;
	}
}

void MX28_ParseCommandLine(unsigned char* cmd)
{
	// commands:
	// 	!X+		enable
	// 	!X-		disable
	// 	!X=n	set position to N
	// 	?X		report position

	unsigned char c0, c;

	c0 = *(cmd++);

	c = toLower(*(cmd++));
	if(c != 'x') {
		// ERROR
		return;
	}

	// get mode
	if(c0 == '?') {
		MX28_Request(MX28_PRESENT_POSITION);
		MX28_Request(MX28_GOAL_POSITION);
		MX28_Request(MX28_MOVING_SPEED);
		MX28_Request(MX28_TORQUE_LIMIT);
	}
	// command mode
	else if(c0 == '!') {
		c = toLower(*(cmd++));

		// enable
		if(c == '+') {
			MX28_IsActive = TRUE;
		}
		// disable
		else if(c == '-') {
			MX28_IsActive = FALSE;
		}
		// set goal position: range=0..4095, delta=~0.088degree
		else if(c == '=') {
			MX28_Set(MX28_GOAL_POSITION, parseLong(&cmd));
		}
		// set moving speed: range=0..1023, delta=~0.114rpm (we use joint mode)
		else if(c == 'v' && *cmd == '=') {
			cmd++;
			MX28_Set(MX28_MOVING_SPEED, parseLong(&cmd));
		}
		// set torque limit: range=0..1023, delta=~0.1%
		else if(c == 't' && *cmd == '=') {
			cmd++;
			MX28_Set(MX28_TORQUE_LIMIT, parseLong(&cmd));
		}
		// auto move
		else if(c == 'a' && *cmd == '+') {
			MX28_auto = 1;
		}
		else if(c == 'a' && *cmd == '-') {
			MX28_auto = 0;
			MX28_Set(MX28_GOAL_POSITION, MX28_POSITION_CENTER);
		}
		// ERROR
		else {
			return;
		}
	}
	// ERROR
	else {
		return;
	}
}
