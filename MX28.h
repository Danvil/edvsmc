
extern long MX28_IsActive;

extern void MX28_Init(void);

extern void MX28_Add_RBR_Char(unsigned char c);

// motor variables
#define MX28_GOAL_POSITION		(30)
#define MX28_MOVING_SPEED		(32)
#define MX28_TORQUE_LIMIT		(34)
#define MX28_PRESENT_POSITION	(36)

/** Sets a motor variable */
extern void MX28_Set(long var, long val);

/** Requests an update for a motor variable */
extern void MX28_Request(long var);

/** Gets the cached value of a motor variable
 * Cached variables are updated by requesting them and
 * waiting for some time...
 */
extern long MX28_Get(long var);

extern void MX28_Update_1(void);
extern void MX28_Update_5(void);
extern void MX28_Update_20(void);

extern void MX28_ParseCommandLine(unsigned char* cmd);
