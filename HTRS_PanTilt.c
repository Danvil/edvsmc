#include "EDVS128_2106.h"

#ifdef INCLUDE_HITEC_ROBOTSERVO

#ifdef INCLUDE_HITEC_SINGLEUART_ROBOTSERVO
  #ifdef INCLUDE_HITEC_DUALUART_ROBOTSERVO
    #error you can only enable either SINGLEUART or DUALUART HiTEC robot servo code
  #endif
#endif
#ifdef INCLUDE_HITEC_DUALUART_ROBOTSERVO
  #ifdef INCLUDE_HITEC_TWI_DIRECTION_ENCODING
    #error you can only enable either TWI_DIRECTION_ENCODING or DUALUART HiTEC robot servo code
  #endif
#endif

long HTRSPT_DesPosition[3];						// -800 .. 0 .. +800
long HTRSPT_CurPosition[3];						// -800 .. 0 .. +800
long HTRSPT_MaxVelocity[3];						// 0 .. +127
long HTRSPT_CenterPosition[3];					// where is the "zero"? by default 1500

#ifdef INCLUDE_HITEC_TWI_DIRECTION_ENCODING
long HRTS_DirectionMultiplier[3];  				// -1 or +1, depending on "mounting" direction so that "up" and "left" is positive
#endif
long HTRS_PanTiltEnableControlFlag;

#ifdef INLCUDE_HITEC_SINMOTION
extern void HTRS_ComputeSinusoidLookup(void);
short SIN_LOOKUP[2048];
long HTRS_SinusoidalMotionIndex[3], HTRS_SinusoidalMotionIncrement[3];
#endif

long HTRS_data0[7], HTRS_data1[7];
long HTRSPointer0, HTRSPointer1;
long HTRS_ServoUpdateRequired;					// this indicates required updates:
	 											   // BIT(0) -> position update for servo0
												   // BIT(1) -> position update for servo1
												   // BIT(2) -> position update for servo2
												   // BIT(3) -> velocity update for servo0
												   // BIT(4) -> velocity update for servo1
												   // BIT(5) -> velocity update for servo2

#ifdef INCLUDE_HITEC_SPI_ROBOTSERVO
unsigned long SPI_UART_LOOKUP[256];				// lookup table to invert bits and swap order
#endif

// *****************************************************************************
void HTRS_PanTiltInit(void) {
#if PLL_CLOCK != 64			  			   // all baud rates calculated for 64MHz
#error HITEC_DUAL_ROBOTSERVO baud rate only valid for 64MHz
#endif

#if defined(INCLUDE_HITEC_SINGLEUART_ROBOTSERVO) || defined(INCLUDE_HITEC_DUALUART_ROBOTSERVO)
// *****************************************************************************
									 		// activate UART1
  UART1_LCR = 0x87;							// Enable the divisor, set 8N2
  UART1_DLM = 0x00;							// Divisor latch MSB (for baud rates < 4800) 
  UART1_DLL = (0x7D); UART1_FDR = (((0x03)<<4) | 0x02);		 // 19200Baud
  UART1_LCR = 0x07;							// Close divisor, set 8N2

  UART1_IER = 0x00;							// disable UART1 interrupts
  UART1_FCR = 0x01;							// enable the fifos
  UART1_FCR = 0x01 | 0x06;					// Reset FIFOs
  UART1_TER = 0x80;							// Enable Transmitter (default)

  PCB_PINSEL0 |= BIT(16) | BIT(18);	  		// enable TxD1, RxD1 output pins
#endif

#ifdef INCLUDE_HITEC_SPI_ROBOTSERVO
// *****************************************************************************
											// here we configure SSP/SPI as "fake UART"
  SCB_PCONP &= ~(BIT(8));					// disable basic SPI
  SCB_PCONP |= (BIT(21));					// enale fancy SSP (emulating SPI)

  SSP_CPSR = 238;							// clock prescaler (>=2, <=254, even!)
  SSP_CR0 = ((13)<<8) | 0x09;				// -> 64MHz / (238 * (13+1)) provides 19207.6831 baud... close enough
  		  						  			// bit 7   - CPHA=0
  		  									// bit 6   - CPOL=0
											// bit 5:4 - frame format: SPISPI mode, 16bit transfer
											// bit 3:0 - data size: 11bit (start+8+stop+stop)

  SSP_CR1  = 0x00; 		  					// master mode, no loopback, disable SPI

  PCB_PINSEL0 |= BIT(10);					// enable MISO
  PCB_PINSEL0 |= BIT(12);					// enable MOSI

  SSP_CR1 |= BIT(1); 		  				// enable SPI interface

// *****************************************************************************
  {
    long in, out;				 	  	   // to compute lookup table at power-up
    for (in=0; in<256; in++) {				// lookup table to invert bits and swap order
      out=0;
      if ((in&0x01)==0) out |= 0x80;
      if ((in&0x02)==0) out |= 0x40;
      if ((in&0x04)==0) out |= 0x20;
      if ((in&0x08)==0) out |= 0x10;
      if ((in&0x10)==0) out |= 0x08;
      if ((in&0x20)==0) out |= 0x04;
      if ((in&0x40)==0) out |= 0x02;
      if ((in&0x80)==0) out |= 0x01;
      SPI_UART_LOOKUP[in]=out;
    }
  }
#endif

#ifdef INCLUDE_HITEC_TWI_DIRECTION_ENCODING
  FGPIO_IODIR &= ~(PIN_HTRS_DIR_ENCODE0 | PIN_HTRS_DIR_ENCODE1);   // direction encoding pins to input
  if (FGPIO_IOPIN & PIN_HTRS_DIR_ENCODE0) {	// revert servo0?
    HRTS_DirectionMultiplier[0] = +1;
  } else {
    HRTS_DirectionMultiplier[0] = -1;
  }
  if (FGPIO_IOPIN & PIN_HTRS_DIR_ENCODE1) {	// revert servo1?
    HRTS_DirectionMultiplier[1] = +1;
  } else {
    HRTS_DirectionMultiplier[1] = -1;
  }
#endif


#ifdef INCLUDE_HITEC_DUALUART_ROBOTSERVO
  FGPIO_IOCLR =  (PIN_HTRS_ENABLE_SERVO0 | PIN_HTRS_ENABLE_SERVO1);	  			// clear pins -> disable servos
  FGPIO_IODIR |= (PIN_HTRS_ENABLE_SERVO0 | PIN_HTRS_ENABLE_SERVO1);

  FGPIO_IOSET =  (PIN_HTRS_ENABLE_SERVO0); 						   				// initially select servo0
#endif

// *****************************************************************************
  HTRSPT_CenterPosition[0]=1500;			// center to default 1500
  HTRSPT_CenterPosition[1]=1500;			// center to default 1500
  HTRSPT_CenterPosition[2]=1500;			// center to default 1500

#ifdef HITEC_PTY_ZEROANGLES
  HTRSPT_CenterPosition[0]=1500-356;		// center to default 1500
  HTRSPT_CenterPosition[1]=1500-394;		// center to default 1500
  HTRSPT_CenterPosition[2]=1500-115;		// center to default 1500
#endif

  HTRS_PanTiltSetDesPosition(0,0);			// set to center
  HTRS_PanTiltSetDesPosition(1,0);
  HTRS_PanTiltSetDesPosition(2,0);

  HTRSPT_CurPosition[0] = -1; 				// unknown
  HTRSPT_CurPosition[1] = -1;				// unknown
  HTRSPT_CurPosition[2] = -1;				// unknown

  HTRSPT_MaxVelocity[0] = 127;
  HTRSPT_MaxVelocity[1] = 127;
  HTRSPT_MaxVelocity[2] = 127;

  HTRS_PanTiltEnableControlFlag = FALSE;	// do not automatically start talking to servos

#ifdef INLCUDE_HITEC_SINMOTION
  HTRS_ComputeSinusoidLookup();	  			// fill lookup table
  HTRS_SinusoidalMotionIndex[0] = 0; 			// point somewhere
  HTRS_SinusoidalMotionIndex[1] = 0; 			// point somewhere
  HTRS_SinusoidalMotionIndex[2] = 0; 			// point somewhere
  HTRS_SinusoidalMotionIncrement[0] = 0;		// no increment
  HTRS_SinusoidalMotionIncrement[1] = 0;		// no increment
  HTRS_SinusoidalMotionIncrement[2] = 0;		// no increment
#endif

  HTRSPointer0 = 0;				  			// start collecting incoming data at beginning of queue
  HTRSPointer1 = 0;				  			// start collecting incoming data at beginning of queue
  HTRS_ServoUpdateRequired = 0;				// no update required
}

// *****************************************************************************
void HTRS_PanTiltEnableControl(unsigned char flag) {
  HTRS_PanTiltEnableControlFlag = flag;
}

// *****************************************************************************
void HTRS_PanTiltSetDesPosition(unsigned char id, long targetPos) {
  if (id>=3) return;
  if (targetPos>+800) targetPos=+800;
  if (targetPos<-800) targetPos=-800;
#ifdef INCLUDE_HITEC_TWI_DIRECTION_ENCODING
  HTRSPT_DesPosition[id] = HTRSPT_CenterPosition[id]+((HRTS_DirectionMultiplier[id])*targetPos);
#else
  HTRSPT_DesPosition[id] = HTRSPT_CenterPosition[id]+(targetPos);
#endif
  HTRS_ServoUpdateRequired |= BIT(id);
}

// *****************************************************************************
long HTRS_PanTiltGetDesPosition(unsigned char id) {
  if (id>=3) return(0);
#ifdef INCLUDE_HITEC_TWI_DIRECTION_ENCODING
  return((HRTS_DirectionMultiplier[id])*(HTRSPT_DesPosition[id]-HTRSPT_CenterPosition[id]));
#else
  return(HTRSPT_DesPosition[id]-HTRSPT_CenterPosition[id]);
#endif
}

// *****************************************************************************
long HTRS_PanTiltGetPosition(unsigned char id) {
  if (id>=3) return(0);
#ifdef INCLUDE_HITEC_TWI_DIRECTION_ENCODING
  return((HRTS_DirectionMultiplier[id])*(HTRSPT_CurPosition[id]-HTRSPT_CenterPosition[id]));
#else
  return(HTRSPT_CurPosition[id]-HTRSPT_CenterPosition[id]);
#endif
}

// *****************************************************************************
void HTRS_PanTiltSetMaxVelocity(unsigned char id, long velocity) {
  if (id>=3) return;
  if (velocity>127) velocity=127;
  if (velocity<  0) velocity=0;
  HTRSPT_MaxVelocity[id] = velocity;
  HTRS_ServoUpdateRequired |= BIT(id+3);
}

// *****************************************************************************
long HTRS_PanTiltGetMaxVelocity(unsigned char id) {
  if (id>=3) return(0);
  return(HTRSPT_MaxVelocity[id]);
}


// *****************************************************************************
#ifdef INLCUDE_HITEC_SINMOTION
void HTRS_SetSinusoidalMotionIncrement(long id, long delta) {
  if (id>4) {
    return;
  }

  if (id==4) {
    HTRS_SinusoidalMotionIncrement[0] = delta;
    HTRS_SinusoidalMotionIncrement[1] = delta;
    HTRS_SinusoidalMotionIncrement[2] = delta;
  } else {
    HTRS_SinusoidalMotionIncrement[id] = delta;
  }
}

long HTRS_GetSinusoidalMotionIncrement(long id) {
  if (id>2) {
    return(0);
  }
  return(HTRS_SinusoidalMotionIncrement[id]);
}

void HTRS_ComputeSinusoidLookup(void) {
  long i=0;
  SIN_LOOKUP[i++]=    0;
  SIN_LOOKUP[i++]=  100;
  SIN_LOOKUP[i++]=  201;
  SIN_LOOKUP[i++]=  301;
  SIN_LOOKUP[i++]=  402;
  SIN_LOOKUP[i++]=  502;
  SIN_LOOKUP[i++]=  603;
  SIN_LOOKUP[i++]=  703;
  SIN_LOOKUP[i++]=  804;
  SIN_LOOKUP[i++]=  904;
  SIN_LOOKUP[i++]= 1005;
  SIN_LOOKUP[i++]= 1105;
  SIN_LOOKUP[i++]= 1206;
  SIN_LOOKUP[i++]= 1306;
  SIN_LOOKUP[i++]= 1407;
  SIN_LOOKUP[i++]= 1507;
  SIN_LOOKUP[i++]= 1607;
  SIN_LOOKUP[i++]= 1708;
  SIN_LOOKUP[i++]= 1808;
  SIN_LOOKUP[i++]= 1909;
  SIN_LOOKUP[i++]= 2009;
  SIN_LOOKUP[i++]= 2109;
  SIN_LOOKUP[i++]= 2210;
  SIN_LOOKUP[i++]= 2310;
  SIN_LOOKUP[i++]= 2410;
  SIN_LOOKUP[i++]= 2510;
  SIN_LOOKUP[i++]= 2611;
  SIN_LOOKUP[i++]= 2711;
  SIN_LOOKUP[i++]= 2811;
  SIN_LOOKUP[i++]= 2911;
  SIN_LOOKUP[i++]= 3011;
  SIN_LOOKUP[i++]= 3111;
  SIN_LOOKUP[i++]= 3211;
  SIN_LOOKUP[i++]= 3311;
  SIN_LOOKUP[i++]= 3411;
  SIN_LOOKUP[i++]= 3511;
  SIN_LOOKUP[i++]= 3611;
  SIN_LOOKUP[i++]= 3711;
  SIN_LOOKUP[i++]= 3811;
  SIN_LOOKUP[i++]= 3911;
  SIN_LOOKUP[i++]= 4011;
  SIN_LOOKUP[i++]= 4110;
  SIN_LOOKUP[i++]= 4210;
  SIN_LOOKUP[i++]= 4310;
  SIN_LOOKUP[i++]= 4409;
  SIN_LOOKUP[i++]= 4509;
  SIN_LOOKUP[i++]= 4609;
  SIN_LOOKUP[i++]= 4708;
  SIN_LOOKUP[i++]= 4808;
  SIN_LOOKUP[i++]= 4907;
  SIN_LOOKUP[i++]= 5006;
  SIN_LOOKUP[i++]= 5106;
  SIN_LOOKUP[i++]= 5205;
  SIN_LOOKUP[i++]= 5304;
  SIN_LOOKUP[i++]= 5403;
  SIN_LOOKUP[i++]= 5503;
  SIN_LOOKUP[i++]= 5602;
  SIN_LOOKUP[i++]= 5701;
  SIN_LOOKUP[i++]= 5800;
  SIN_LOOKUP[i++]= 5898;
  SIN_LOOKUP[i++]= 5997;
  SIN_LOOKUP[i++]= 6096;
  SIN_LOOKUP[i++]= 6195;
  SIN_LOOKUP[i++]= 6294;
  SIN_LOOKUP[i++]= 6392;
  SIN_LOOKUP[i++]= 6491;
  SIN_LOOKUP[i++]= 6589;
  SIN_LOOKUP[i++]= 6688;
  SIN_LOOKUP[i++]= 6786;
  SIN_LOOKUP[i++]= 6884;
  SIN_LOOKUP[i++]= 6983;
  SIN_LOOKUP[i++]= 7081;
  SIN_LOOKUP[i++]= 7179;
  SIN_LOOKUP[i++]= 7277;
  SIN_LOOKUP[i++]= 7375;
  SIN_LOOKUP[i++]= 7473;
  SIN_LOOKUP[i++]= 7571;
  SIN_LOOKUP[i++]= 7669;
  SIN_LOOKUP[i++]= 7766;
  SIN_LOOKUP[i++]= 7864;
  SIN_LOOKUP[i++]= 7961;
  SIN_LOOKUP[i++]= 8059;
  SIN_LOOKUP[i++]= 8156;
  SIN_LOOKUP[i++]= 8254;
  SIN_LOOKUP[i++]= 8351;
  SIN_LOOKUP[i++]= 8448;
  SIN_LOOKUP[i++]= 8545;
  SIN_LOOKUP[i++]= 8642;
  SIN_LOOKUP[i++]= 8739;
  SIN_LOOKUP[i++]= 8836;
  SIN_LOOKUP[i++]= 8933;
  SIN_LOOKUP[i++]= 9029;
  SIN_LOOKUP[i++]= 9126;
  SIN_LOOKUP[i++]= 9223;
  SIN_LOOKUP[i++]= 9319;
  SIN_LOOKUP[i++]= 9415;
  SIN_LOOKUP[i++]= 9512;
  SIN_LOOKUP[i++]= 9608;
  SIN_LOOKUP[i++]= 9704;
  SIN_LOOKUP[i++]= 9800;
  SIN_LOOKUP[i++]= 9896;
  SIN_LOOKUP[i++]= 9991;
  SIN_LOOKUP[i++]=10087;
  SIN_LOOKUP[i++]=10183;
  SIN_LOOKUP[i++]=10278;
  SIN_LOOKUP[i++]=10374;
  SIN_LOOKUP[i++]=10469;
  SIN_LOOKUP[i++]=10564;
  SIN_LOOKUP[i++]=10659;
  SIN_LOOKUP[i++]=10754;
  SIN_LOOKUP[i++]=10849;
  SIN_LOOKUP[i++]=10944;
  SIN_LOOKUP[i++]=11039;
  SIN_LOOKUP[i++]=11133;
  SIN_LOOKUP[i++]=11228;
  SIN_LOOKUP[i++]=11322;
  SIN_LOOKUP[i++]=11416;
  SIN_LOOKUP[i++]=11511;
  SIN_LOOKUP[i++]=11605;
  SIN_LOOKUP[i++]=11699;
  SIN_LOOKUP[i++]=11793;
  SIN_LOOKUP[i++]=11886;
  SIN_LOOKUP[i++]=11980;
  SIN_LOOKUP[i++]=12073;
  SIN_LOOKUP[i++]=12167;
  SIN_LOOKUP[i++]=12260;
  SIN_LOOKUP[i++]=12353;
  SIN_LOOKUP[i++]=12446;
  SIN_LOOKUP[i++]=12539;
  SIN_LOOKUP[i++]=12632;
  SIN_LOOKUP[i++]=12725;
  SIN_LOOKUP[i++]=12817;
  SIN_LOOKUP[i++]=12910;
  SIN_LOOKUP[i++]=13002;
  SIN_LOOKUP[i++]=13094;
  SIN_LOOKUP[i++]=13186;
  SIN_LOOKUP[i++]=13278;
  SIN_LOOKUP[i++]=13370;
  SIN_LOOKUP[i++]=13462;
  SIN_LOOKUP[i++]=13554;
  SIN_LOOKUP[i++]=13645;
  SIN_LOOKUP[i++]=13736;
  SIN_LOOKUP[i++]=13828;
  SIN_LOOKUP[i++]=13919;
  SIN_LOOKUP[i++]=14010;
  SIN_LOOKUP[i++]=14100;
  SIN_LOOKUP[i++]=14191;
  SIN_LOOKUP[i++]=14282;
  SIN_LOOKUP[i++]=14372;
  SIN_LOOKUP[i++]=14462;
  SIN_LOOKUP[i++]=14552;
  SIN_LOOKUP[i++]=14642;
  SIN_LOOKUP[i++]=14732;
  SIN_LOOKUP[i++]=14822;
  SIN_LOOKUP[i++]=14912;
  SIN_LOOKUP[i++]=15001;
  SIN_LOOKUP[i++]=15090;
  SIN_LOOKUP[i++]=15180;
  SIN_LOOKUP[i++]=15269;
  SIN_LOOKUP[i++]=15357;
  SIN_LOOKUP[i++]=15446;
  SIN_LOOKUP[i++]=15535;
  SIN_LOOKUP[i++]=15623;
  SIN_LOOKUP[i++]=15712;
  SIN_LOOKUP[i++]=15800;
  SIN_LOOKUP[i++]=15888;
  SIN_LOOKUP[i++]=15976;
  SIN_LOOKUP[i++]=16063;
  SIN_LOOKUP[i++]=16151;
  SIN_LOOKUP[i++]=16238;
  SIN_LOOKUP[i++]=16325;
  SIN_LOOKUP[i++]=16413;
  SIN_LOOKUP[i++]=16499;
  SIN_LOOKUP[i++]=16586;
  SIN_LOOKUP[i++]=16673;
  SIN_LOOKUP[i++]=16759;
  SIN_LOOKUP[i++]=16846;
  SIN_LOOKUP[i++]=16932;
  SIN_LOOKUP[i++]=17018;
  SIN_LOOKUP[i++]=17104;
  SIN_LOOKUP[i++]=17189;
  SIN_LOOKUP[i++]=17275;
  SIN_LOOKUP[i++]=17360;
  SIN_LOOKUP[i++]=17445;
  SIN_LOOKUP[i++]=17530;
  SIN_LOOKUP[i++]=17615;
  SIN_LOOKUP[i++]=17700;
  SIN_LOOKUP[i++]=17784;
  SIN_LOOKUP[i++]=17869;
  SIN_LOOKUP[i++]=17953;
  SIN_LOOKUP[i++]=18037;
  SIN_LOOKUP[i++]=18121;
  SIN_LOOKUP[i++]=18204;
  SIN_LOOKUP[i++]=18288;
  SIN_LOOKUP[i++]=18371;
  SIN_LOOKUP[i++]=18454;
  SIN_LOOKUP[i++]=18537;
  SIN_LOOKUP[i++]=18620;
  SIN_LOOKUP[i++]=18703;
  SIN_LOOKUP[i++]=18785;
  SIN_LOOKUP[i++]=18868;
  SIN_LOOKUP[i++]=18950;
  SIN_LOOKUP[i++]=19032;
  SIN_LOOKUP[i++]=19113;
  SIN_LOOKUP[i++]=19195;
  SIN_LOOKUP[i++]=19276;
  SIN_LOOKUP[i++]=19358;
  SIN_LOOKUP[i++]=19439;
  SIN_LOOKUP[i++]=19519;
  SIN_LOOKUP[i++]=19600;
  SIN_LOOKUP[i++]=19681;
  SIN_LOOKUP[i++]=19761;
  SIN_LOOKUP[i++]=19841;
  SIN_LOOKUP[i++]=19921;
  SIN_LOOKUP[i++]=20001;
  SIN_LOOKUP[i++]=20080;
  SIN_LOOKUP[i++]=20159;
  SIN_LOOKUP[i++]=20239;
  SIN_LOOKUP[i++]=20318;
  SIN_LOOKUP[i++]=20396;
  SIN_LOOKUP[i++]=20475;
  SIN_LOOKUP[i++]=20553;
  SIN_LOOKUP[i++]=20631;
  SIN_LOOKUP[i++]=20709;
  SIN_LOOKUP[i++]=20787;
  SIN_LOOKUP[i++]=20865;
  SIN_LOOKUP[i++]=20942;
  SIN_LOOKUP[i++]=21020;
  SIN_LOOKUP[i++]=21097;
  SIN_LOOKUP[i++]=21173;
  SIN_LOOKUP[i++]=21250;
  SIN_LOOKUP[i++]=21326;
  SIN_LOOKUP[i++]=21403;
  SIN_LOOKUP[i++]=21479;
  SIN_LOOKUP[i++]=21555;
  SIN_LOOKUP[i++]=21630;
  SIN_LOOKUP[i++]=21706;
  SIN_LOOKUP[i++]=21781;
  SIN_LOOKUP[i++]=21856;
  SIN_LOOKUP[i++]=21931;
  SIN_LOOKUP[i++]=22005;
  SIN_LOOKUP[i++]=22080;
  SIN_LOOKUP[i++]=22154;
  SIN_LOOKUP[i++]=22228;
  SIN_LOOKUP[i++]=22301;
  SIN_LOOKUP[i++]=22375;
  SIN_LOOKUP[i++]=22448;
  SIN_LOOKUP[i++]=22521;
  SIN_LOOKUP[i++]=22594;
  SIN_LOOKUP[i++]=22667;
  SIN_LOOKUP[i++]=22740;
  SIN_LOOKUP[i++]=22812;
  SIN_LOOKUP[i++]=22884;
  SIN_LOOKUP[i++]=22956;
  SIN_LOOKUP[i++]=23027;
  SIN_LOOKUP[i++]=23099;
  SIN_LOOKUP[i++]=23170;
  SIN_LOOKUP[i++]=23241;
  SIN_LOOKUP[i++]=23312;
  SIN_LOOKUP[i++]=23382;
  SIN_LOOKUP[i++]=23453;
  SIN_LOOKUP[i++]=23523;
  SIN_LOOKUP[i++]=23593;
  SIN_LOOKUP[i++]=23662;
  SIN_LOOKUP[i++]=23732;
  SIN_LOOKUP[i++]=23801;
  SIN_LOOKUP[i++]=23870;
  SIN_LOOKUP[i++]=23939;
  SIN_LOOKUP[i++]=24007;
  SIN_LOOKUP[i++]=24075;
  SIN_LOOKUP[i++]=24144;
  SIN_LOOKUP[i++]=24211;
  SIN_LOOKUP[i++]=24279;
  SIN_LOOKUP[i++]=24346;
  SIN_LOOKUP[i++]=24414;
  SIN_LOOKUP[i++]=24480;
  SIN_LOOKUP[i++]=24547;
  SIN_LOOKUP[i++]=24614;
  SIN_LOOKUP[i++]=24680;
  SIN_LOOKUP[i++]=24746;
  SIN_LOOKUP[i++]=24812;
  SIN_LOOKUP[i++]=24877;
  SIN_LOOKUP[i++]=24943;
  SIN_LOOKUP[i++]=25008;
  SIN_LOOKUP[i++]=25073;
  SIN_LOOKUP[i++]=25137;
  SIN_LOOKUP[i++]=25201;
  SIN_LOOKUP[i++]=25266;
  SIN_LOOKUP[i++]=25330;
  SIN_LOOKUP[i++]=25393;
  SIN_LOOKUP[i++]=25457;
  SIN_LOOKUP[i++]=25520;
  SIN_LOOKUP[i++]=25583;
  SIN_LOOKUP[i++]=25645;
  SIN_LOOKUP[i++]=25708;
  SIN_LOOKUP[i++]=25770;
  SIN_LOOKUP[i++]=25832;
  SIN_LOOKUP[i++]=25894;
  SIN_LOOKUP[i++]=25955;
  SIN_LOOKUP[i++]=26016;
  SIN_LOOKUP[i++]=26077;
  SIN_LOOKUP[i++]=26138;
  SIN_LOOKUP[i++]=26199;
  SIN_LOOKUP[i++]=26259;
  SIN_LOOKUP[i++]=26319;
  SIN_LOOKUP[i++]=26379;
  SIN_LOOKUP[i++]=26438;
  SIN_LOOKUP[i++]=26498;
  SIN_LOOKUP[i++]=26557;
  SIN_LOOKUP[i++]=26615;
  SIN_LOOKUP[i++]=26674;
  SIN_LOOKUP[i++]=26732;
  SIN_LOOKUP[i++]=26790;
  SIN_LOOKUP[i++]=26848;
  SIN_LOOKUP[i++]=26905;
  SIN_LOOKUP[i++]=26963;
  SIN_LOOKUP[i++]=27020;
  SIN_LOOKUP[i++]=27076;
  SIN_LOOKUP[i++]=27133;
  SIN_LOOKUP[i++]=27189;
  SIN_LOOKUP[i++]=27245;
  SIN_LOOKUP[i++]=27301;
  SIN_LOOKUP[i++]=27356;
  SIN_LOOKUP[i++]=27411;
  SIN_LOOKUP[i++]=27466;
  SIN_LOOKUP[i++]=27521;
  SIN_LOOKUP[i++]=27576;
  SIN_LOOKUP[i++]=27630;
  SIN_LOOKUP[i++]=27684;
  SIN_LOOKUP[i++]=27737;
  SIN_LOOKUP[i++]=27791;
  SIN_LOOKUP[i++]=27844;
  SIN_LOOKUP[i++]=27897;
  SIN_LOOKUP[i++]=27949;
  SIN_LOOKUP[i++]=28002;
  SIN_LOOKUP[i++]=28054;
  SIN_LOOKUP[i++]=28106;
  SIN_LOOKUP[i++]=28157;
  SIN_LOOKUP[i++]=28208;
  SIN_LOOKUP[i++]=28259;
  SIN_LOOKUP[i++]=28310;
  SIN_LOOKUP[i++]=28361;
  SIN_LOOKUP[i++]=28411;
  SIN_LOOKUP[i++]=28461;
  SIN_LOOKUP[i++]=28511;
  SIN_LOOKUP[i++]=28560;
  SIN_LOOKUP[i++]=28609;
  SIN_LOOKUP[i++]=28658;
  SIN_LOOKUP[i++]=28707;
  SIN_LOOKUP[i++]=28755;
  SIN_LOOKUP[i++]=28803;
  SIN_LOOKUP[i++]=28851;
  SIN_LOOKUP[i++]=28898;
  SIN_LOOKUP[i++]=28946;
  SIN_LOOKUP[i++]=28993;
  SIN_LOOKUP[i++]=29039;
  SIN_LOOKUP[i++]=29086;
  SIN_LOOKUP[i++]=29132;
  SIN_LOOKUP[i++]=29178;
  SIN_LOOKUP[i++]=29223;
  SIN_LOOKUP[i++]=29269;
  SIN_LOOKUP[i++]=29314;
  SIN_LOOKUP[i++]=29359;
  SIN_LOOKUP[i++]=29403;
  SIN_LOOKUP[i++]=29447;
  SIN_LOOKUP[i++]=29491;
  SIN_LOOKUP[i++]=29535;
  SIN_LOOKUP[i++]=29578;
  SIN_LOOKUP[i++]=29621;
  SIN_LOOKUP[i++]=29664;
  SIN_LOOKUP[i++]=29707;
  SIN_LOOKUP[i++]=29749;
  SIN_LOOKUP[i++]=29791;
  SIN_LOOKUP[i++]=29833;
  SIN_LOOKUP[i++]=29874;
  SIN_LOOKUP[i++]=29915;
  SIN_LOOKUP[i++]=29956;
  SIN_LOOKUP[i++]=29997;
  SIN_LOOKUP[i++]=30037;
  SIN_LOOKUP[i++]=30077;
  SIN_LOOKUP[i++]=30117;
  SIN_LOOKUP[i++]=30156;
  SIN_LOOKUP[i++]=30196;
  SIN_LOOKUP[i++]=30235;
  SIN_LOOKUP[i++]=30273;
  SIN_LOOKUP[i++]=30312;
  SIN_LOOKUP[i++]=30350;
  SIN_LOOKUP[i++]=30387;
  SIN_LOOKUP[i++]=30425;
  SIN_LOOKUP[i++]=30462;
  SIN_LOOKUP[i++]=30499;
  SIN_LOOKUP[i++]=30535;
  SIN_LOOKUP[i++]=30572;
  SIN_LOOKUP[i++]=30608;
  SIN_LOOKUP[i++]=30644;
  SIN_LOOKUP[i++]=30679;
  SIN_LOOKUP[i++]=30714;
  SIN_LOOKUP[i++]=30749;
  SIN_LOOKUP[i++]=30784;
  SIN_LOOKUP[i++]=30818;
  SIN_LOOKUP[i++]=30852;
  SIN_LOOKUP[i++]=30886;
  SIN_LOOKUP[i++]=30919;
  SIN_LOOKUP[i++]=30952;
  SIN_LOOKUP[i++]=30985;
  SIN_LOOKUP[i++]=31018;
  SIN_LOOKUP[i++]=31050;
  SIN_LOOKUP[i++]=31082;
  SIN_LOOKUP[i++]=31114;
  SIN_LOOKUP[i++]=31145;
  SIN_LOOKUP[i++]=31176;
  SIN_LOOKUP[i++]=31207;
  SIN_LOOKUP[i++]=31237;
  SIN_LOOKUP[i++]=31268;
  SIN_LOOKUP[i++]=31298;
  SIN_LOOKUP[i++]=31327;
  SIN_LOOKUP[i++]=31357;
  SIN_LOOKUP[i++]=31386;
  SIN_LOOKUP[i++]=31414;
  SIN_LOOKUP[i++]=31443;
  SIN_LOOKUP[i++]=31471;
  SIN_LOOKUP[i++]=31499;
  SIN_LOOKUP[i++]=31526;
  SIN_LOOKUP[i++]=31554;
  SIN_LOOKUP[i++]=31581;
  SIN_LOOKUP[i++]=31607;
  SIN_LOOKUP[i++]=31634;
  SIN_LOOKUP[i++]=31660;
  SIN_LOOKUP[i++]=31685;
  SIN_LOOKUP[i++]=31711;
  SIN_LOOKUP[i++]=31736;
  SIN_LOOKUP[i++]=31761;
  SIN_LOOKUP[i++]=31785;
  SIN_LOOKUP[i++]=31810;
  SIN_LOOKUP[i++]=31834;
  SIN_LOOKUP[i++]=31857;
  SIN_LOOKUP[i++]=31881;
  SIN_LOOKUP[i++]=31904;
  SIN_LOOKUP[i++]=31927;
  SIN_LOOKUP[i++]=31949;
  SIN_LOOKUP[i++]=31971;
  SIN_LOOKUP[i++]=31993;
  SIN_LOOKUP[i++]=32015;
  SIN_LOOKUP[i++]=32036;
  SIN_LOOKUP[i++]=32057;
  SIN_LOOKUP[i++]=32078;
  SIN_LOOKUP[i++]=32098;
  SIN_LOOKUP[i++]=32118;
  SIN_LOOKUP[i++]=32138;
  SIN_LOOKUP[i++]=32157;
  SIN_LOOKUP[i++]=32176;
  SIN_LOOKUP[i++]=32195;
  SIN_LOOKUP[i++]=32214;
  SIN_LOOKUP[i++]=32232;
  SIN_LOOKUP[i++]=32250;
  SIN_LOOKUP[i++]=32268;
  SIN_LOOKUP[i++]=32285;
  SIN_LOOKUP[i++]=32302;
  SIN_LOOKUP[i++]=32319;
  SIN_LOOKUP[i++]=32335;
  SIN_LOOKUP[i++]=32351;
  SIN_LOOKUP[i++]=32367;
  SIN_LOOKUP[i++]=32383;
  SIN_LOOKUP[i++]=32398;
  SIN_LOOKUP[i++]=32413;
  SIN_LOOKUP[i++]=32427;
  SIN_LOOKUP[i++]=32442;
  SIN_LOOKUP[i++]=32456;
  SIN_LOOKUP[i++]=32469;
  SIN_LOOKUP[i++]=32483;
  SIN_LOOKUP[i++]=32496;
  SIN_LOOKUP[i++]=32509;
  SIN_LOOKUP[i++]=32521;
  SIN_LOOKUP[i++]=32533;
  SIN_LOOKUP[i++]=32545;
  SIN_LOOKUP[i++]=32557;
  SIN_LOOKUP[i++]=32568;
  SIN_LOOKUP[i++]=32579;
  SIN_LOOKUP[i++]=32589;
  SIN_LOOKUP[i++]=32600;
  SIN_LOOKUP[i++]=32610;
  SIN_LOOKUP[i++]=32619;
  SIN_LOOKUP[i++]=32629;
  SIN_LOOKUP[i++]=32638;
  SIN_LOOKUP[i++]=32647;
  SIN_LOOKUP[i++]=32655;
  SIN_LOOKUP[i++]=32663;
  SIN_LOOKUP[i++]=32671;
  SIN_LOOKUP[i++]=32679;
  SIN_LOOKUP[i++]=32686;
  SIN_LOOKUP[i++]=32693;
  SIN_LOOKUP[i++]=32700;
  SIN_LOOKUP[i++]=32706;
  SIN_LOOKUP[i++]=32712;
  SIN_LOOKUP[i++]=32718;
  SIN_LOOKUP[i++]=32723;
  SIN_LOOKUP[i++]=32728;
  SIN_LOOKUP[i++]=32733;
  SIN_LOOKUP[i++]=32737;
  SIN_LOOKUP[i++]=32741;
  SIN_LOOKUP[i++]=32745;
  SIN_LOOKUP[i++]=32749;
  SIN_LOOKUP[i++]=32752;
  SIN_LOOKUP[i++]=32755;
  SIN_LOOKUP[i++]=32758;
  SIN_LOOKUP[i++]=32760;
  SIN_LOOKUP[i++]=32762;
  SIN_LOOKUP[i++]=32764;
  SIN_LOOKUP[i++]=32765;
  SIN_LOOKUP[i++]=32766;
  SIN_LOOKUP[i++]=32767;
  SIN_LOOKUP[i++]=32767;
  SIN_LOOKUP[i++]=32767;			  // note: this shold be 32768, but doesn't fit into "short"
  
  for (; i<1024; i++) {		  	 	 	 // mirror sinusoid around peak
    SIN_LOOKUP[i]=SIN_LOOKUP[1024-i];  	  // peak
  }
  for (i=1024; i<2048; i++) { 	 	 	 	  // mirror sinusoid
    SIN_LOOKUP[i] = -SIN_LOOKUP[i-1024];
  }
}
#endif

#endif
